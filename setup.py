# -*- coding: utf-8 -*-
import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "oook_replace",
    version = "0",
    author = "Étienne Loks",
    author_email = "etienne.loks_AT_peacefrogsDOTnet",
    description = ("XXX"),
    license = "AGPL-3",
    keywords = "XXX",
    url = "XXX",
    #XXX: Should we be distributing the test_project?
    #packages=['oook_replace', 'test_project'],
    packages=['oook_replace'],
    #long_description=read('XXX'),
    classifiers=[
    ],
)
