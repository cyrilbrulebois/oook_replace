#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import bs4
from cStringIO import StringIO
import locale, os, re
from lxml import etree
from PIL import Image
from xml.etree.cElementTree import ElementTree, fromstring
from xml.sax.saxutils import escape
from zipfile import ZipFile, ZIP_DEFLATED

from django.conf import settings
from django.template.defaultfilters import slugify
from django.template.loader import render_to_string
from oook_translation import OOOTranslation

def translate_context(context, locale):
    oook_translation = OOOTranslation.get_oook_translation()
    if locale not in oook_translation:
        return context
    new_context = {}
    for k in context:
        new_key = k
        if k in oook_translation[locale]:
            new_key = oook_translation[locale][k]
        new_context[new_key] = context[k]
    return new_context

OOO_NS = "{urn:oasis:names:tc:opendocument:xmlns:text:1.0}"

def _set_value_from_formula(value, context, default_value):
    value = value.strip()
    if value.startswith("ooow:") and len(value) >= 5:
        value = value[5:]
    if value.startswith('"') and value.endswith('"') and len(value) > 1:
        value = value[1:-1]
    elif value in context:
        value, ext = _format_value(context[value], default_value)
    else:
        value = None
    return value

def _parse_condition(condition, context, default_value):
    # parse only == and != operator
    operator = ""
    if "!=" in condition:
        operator = "!="
    elif "==" in condition:
        operator = "=="
    else:
        return
    var1, var2 = condition.split(operator)
    var1 = _set_value_from_formula(var1, context, default_value)
    var2 = _set_value_from_formula(var2, context, default_value)
    res = var1 == var2
    if operator == '!=':
        res = not res
    return res

def _format_value(value, default_value):
    if hasattr(value, 'strftime'):
        c_locale = settings.LANGUAGE_CODE.split('-')
        if len(c_locale) == 2:
            c_locale[1] = c_locale[1].upper()
        c_locale = "_".join(c_locale)
        if locale.getlocale()[0] != c_locale:
            for loc in (c_locale, c_locale+'.utf8'):
                try:
                    locale.setlocale(locale.LC_ALL, loc)
                    break
                except:
                    pass
        try:
            if hasattr(settings, 'OOOK_DATE_FORMAT') \
               and settings.OOOK_DATE_FORMAT:
                value = value.strftime(settings.OOOK_DATE_FORMAT).lower()
            else:
                value = value.strftime('%x')
        except ValueError:
            value = unicode(value)
        if locale.getlocale()[1]:
            value = value.decode(locale.getlocale()[1])
    value = escape(unicode(value)) if value else default_value
    return value, {}

def _format_image(value, default_value):
    # value: {'path':xxxx, 'dim_x':xxx; 'dim_y':xxx} or path
    dim_x, dim_y = None, None
    if type(value) == dict:
        if 'path' not in value:
            return '', {}
        if 'dim_x' in value:
            dim_x = value['dim_x']
        if 'dim_y' in value:
            dim_y = value['dim_y']
        value = value['path']
    try:
        cimg = Image.open(value)
        width, height = None, None
        if dim_x:
            try:
                width = int(dim_x)
            except ValueError:
                pass
        if dim_y:
            try:
                height = int(dim_y)
            except ValueError:
                pass

        cwidth, cheight = cimg.size
        # convert pixel to centimeters:
        # 2.54 centimeters/inch, 96 pixels/inch
        cwidth = round(cwidth * 2.54 / 96, 2)
        cheight = round(cheight * 2.54 / 96, 2)
        if not cheight:
            return '', {}
        ratio = cwidth/cheight
        if not height and not width:
            height = cheight
            width = cwidth
        if height and not width:
            width = height * ratio
        elif width and not height:
            height = width / ratio
        if hasattr(settings, 'PAGE_DIM'):
            max_width, max_height = settings.PAGE_DIM[0] - 3,\
                                    settings.PAGE_DIM[1] - 3
            if width > max_width or height > max_height:
                height = round(max_width * (height/width))
                width = max_width
        ext = value.split('.')[-1]
        name = slugify(value[:-len(ext)].split(os.sep)[-1]).replace('-', '')
        dest_filename = "Pictures" + os.sep + name + "." + ext
        dct = {"image_name":name,
               "image_file":dest_filename,
               "width":width,
               "height":height}
        img = _clean_and_regroup(render_to_string("oook/oook-image.xml", dct))
        return unicode(img), {'image':(value, dest_filename)}
    except IOError:
        return '', {}

def _format_table(value, default_value):
    dct = {'tablename':value['tablename'],
           'table':value['items']}
    dct['cols'] = value.get('cols')
    dct['width'] = value.get('width')
    dct['col_number'] = value.get('col_number')
    dct['header_rows'] = value.get('header_rows') or 1
    if not dct['col_number'] and value['items']:
        dct['col_number'] = len(value['items'][0])
    table = _clean_and_regroup(render_to_string("oook/oook-table.xml", dct))
    style = _clean_and_regroup(render_to_string("oook/oook-table-style.xml", dct))
    return table, {'style':style}

def _clean_and_regroup(value):
    return u''.join([line.strip() for line in value.split('\n') if line.strip()])

VAR_EXPR = u"###%sVAR%s %s###"
IF_EXPR = u"###%sIF%s %s###(.*)###ENDIF###"
IMG_EXPR = u"###%sIMAGE%s %s###"
TABLE_EXPR = u"###%sTABLE%s %s###"

WHOLE_KEY_FILTER = u"((?:(?: )*(?:<[^#>]*>)*(?: )*(?:[-a-zA-Z0-9_])*(?: )*)*)"
WHOLE_TAG_FILTER = u"((?:[^#]*>)*)"
WHOLE_KEY_FILTER = u"([^#]*)"

RE_VAR = re.compile(VAR_EXPR % (WHOLE_TAG_FILTER, WHOLE_TAG_FILTER,
                                WHOLE_KEY_FILTER))
RE_IF = re.compile(IF_EXPR % (WHOLE_TAG_FILTER, WHOLE_TAG_FILTER,
                              WHOLE_KEY_FILTER))
RE_IMG = re.compile(IMG_EXPR % (WHOLE_TAG_FILTER, WHOLE_TAG_FILTER,
                                WHOLE_KEY_FILTER))
RE_TABLE = re.compile(TABLE_EXPR % (WHOLE_TAG_FILTER, WHOLE_TAG_FILTER,
                                    WHOLE_KEY_FILTER))

TAG_FILTER = re.compile(u"(<[^<^>]*>)")
KEY_FILTER = re.compile(u"([-a-zA-Z0-9_]*)")

def _filter_key(base_key):
    # return (key, extra_marker)
    # manage strange key such as:
    # test_<text:span text:style-name="T1">date</text:span>
    key = base_key[:]
    key = key.strip()
    tags, new_key = '', key[:]
    for tag in TAG_FILTER.findall(key):
        tags += tag
        new_key = new_key.replace(tag, '')
    full_key = ''
    for k in KEY_FILTER.findall(new_key):
        if not k:
            continue
        full_key += k
    return full_key, tags

def _update_extra(extra, new_extra):
    for k in new_extra:
        if k not in extra:
            extra[k] = []
        if not isinstance(new_extra[k], list):
            new_extra[k] = [new_extra[k]]
        extra[k] += new_extra[k]
    return extra

def __parser(regexp, sub_exp, treatment):
    def parse(context, value, default_value=''):
        extra = {}
        for prepre_tag, pre_tag, base_key in regexp.findall(value[:]):
            v, val = "", None
            key, extra_markers = _filter_key(base_key)
            if prepre_tag:
                v += prepre_tag
            if pre_tag:
                v += pre_tag
            if key in context and context[key]:
                new_value, ext = treatment(context[key], default_value)
                _update_extra(extra, ext)
                v += new_value
            # to preserve a consistent OOO file put extra_markers
            if extra_markers:
                v += extra_markers
            value = re.sub(sub_exp % (prepre_tag, pre_tag, base_key), v, value)
        return value, extra
    return parse

_var_parsing = __parser(RE_VAR, VAR_EXPR, _format_value)
_image_parsing = __parser(RE_IMG, IMG_EXPR, _format_image)
_table_parsing = __parser(RE_TABLE, TABLE_EXPR, _format_table)

def _custom_parsing(context, value, default_value=''):
    """
    ###IF nom_var### ###ENDIF### for conditionnal display
    Be carreful nested condition are not yet managed!
    """
    regexp, sub_exp = RE_IF, IF_EXPR
    extra = {}
    for prepre_tag, pre_tag, base_key, val in regexp.findall(value[:]):
        v = ""
        key, extra_markers = _filter_key(base_key)
        if prepre_tag:
            v += prepre_tag
        if pre_tag:
            v += pre_tag
        if key in context and context[key]:
            cv, ext = _custom_parsing(context, val, default_value)
            _update_extra(extra, ext)
            v += cv
        # to preserve a consistent OOO file put extra_markers
        if extra_markers:
            v += extra_markers
        value = re.sub(sub_exp % (prepre_tag, pre_tag, base_key), v, value)
    value, ext = _var_parsing(context, value, default_value)
    _update_extra(extra, ext)
    value, ext = _image_parsing(context, value, default_value)
    _update_extra(extra, ext)
    value, ext = _table_parsing(context, value, default_value)
    _update_extra(extra, ext)
    return value, extra

EXTRA_NAMESPACES = [
    'xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"',
    'xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"',
    'xmlns:xlink="http://www.w3.org/1999/xlink"']

def _addnamespaces(content):
    idx = content.index(' xmlns:')
    for namespace in EXTRA_NAMESPACES:
        if namespace in content:
            continue
        content = content[:idx] + ' ' + namespace + content[idx:]
    return content

def _oook_replace(content, context, missing_keys, default_value=''):
    # regular ooo parsing
    for xp in ('variable-set', 'variable-get'):
        for p in content.findall(".//"+OOO_NS+xp):
            name = p.get(OOO_NS+"name")
            if name in context:
                value = context[name]
                p.text, ext = _format_value(value, default_value)
            else:
                if default_value != None:
                    p.text = default_value
                missing_keys.add(name)
    for p in content.findall(".//"+OOO_NS+"conditional-text"):
        condition = p.get(OOO_NS+"condition")
        res = 'true' if _parse_condition(condition, context, default_value) \
              else 'false'
        value = p.get(OOO_NS+'string-value-if-' + res)
        value, ext = _format_value(value, default_value)
        if value.strip() in context:
            value = context[value.strip()]
        p.text = value

    # raw content parsing
    str_io = StringIO()
    content.write(str_io)
    value = str_io.getvalue()

    value = _addnamespaces(value)
    value, extra = _custom_parsing(context, value, default_value)
    value = value.encode('utf-8')
    value = _clean_xml(value)
    if 'style' in extra:
        for style in extra['style']:
            value = _insert_style(value, style)

    if 'image' not in extra:
        extra['image'] = []
    return value, extra['image']

def _clean_xml(value):
    value = unicode(bs4.BeautifulSoup(value.decode('utf-8'), "xml")
                                                    ).encode('utf-8')
    tree = etree.parse(StringIO(value))
    # in ooo templates ###TABLE ### are inside text:p tags: after replacement
    # by table we can fix it
    for XPATH in ("//text:p/table:table", "//text:span/table:table",
                  "//text:p/table:table",):
        tables = tree.xpath(XPATH,
          namespaces={'table':'urn:oasis:names:tc:opendocument:xmlns:table:1.0',
                      'text':"urn:oasis:names:tc:opendocument:xmlns:text:1.0"})
        for table in tables:
            p = table.getparent()
            parent = p.getparent()
            parent.replace(p, table)
    value = etree.tostring(tree)
    return value

def _insert_style(value, style):
    if not style:
        return value
    # clean and regroup
    style = u''.join([line for line in style.split('\n') if line.strip()])
    tree = etree.parse(StringIO(value))
    xmlstyle = tree.xpath("//office:automatic-styles",
        namespaces={'office':"urn:oasis:names:tc:opendocument:xmlns:office:1.0"})
    if not xmlstyle:
        base = tree.xpath('//office:document-content',
            namespaces={'office':"urn:oasis:names:tc:opendocument:xmlns:office:1.0",})
        base.append(etree.Element("office:automatic-styles"))
        base.append(etree.XML(style))
    else:
        xmlstyle = xmlstyle[0]
        for child in etree.XML(style).getchildren():
            xmlstyle.append(child)
    value = etree.tostring(tree)
    return value

def oook_replace(infile, outfile, context, default_value=''):
    inzip = ZipFile(infile, 'r', ZIP_DEFLATED)
    outzip = ZipFile(outfile, 'w', ZIP_DEFLATED)

    values = {}
    missing_keys = set()
    images = []
    for xml_file in ('content.xml', 'styles.xml'):
        content = ElementTree(fromstring(inzip.read(xml_file)))
        content, imgs = _oook_replace(content, context, missing_keys,
                                        default_value)
        images += imgs
        values[xml_file] = content

    for f in inzip.infolist():
        if f.filename in values:
            outzip.writestr(f.filename, values[f.filename])
        else:
            outzip.writestr(f, inzip.read(f.filename))
    for src_filename, dest_filename in images:
        outzip.writestr(dest_filename, open(src_filename).read())
    inzip.close()
    outzip.close()
    return missing_keys
