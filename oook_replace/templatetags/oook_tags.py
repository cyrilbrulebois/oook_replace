#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string
from django import template

register = template.Library()

def toalpha(value):
    # 0 -> A, 1 -> B, ..., 26 -> AA, ...
    ascii_len = len(string.ascii_uppercase)
    val = ''
    value = int(value)
    prefix = value/ascii_len
    if value >= ascii_len:
        if prefix > ascii_len:
            # do not manage number higher than 26*26
            return ""
        val += string.ascii_uppercase[prefix]
        value = value % ascii_len
    val += string.ascii_uppercase[value]
    return val
register.filter('toalpha', toalpha)
