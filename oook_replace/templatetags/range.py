#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library

register = Library()

@register.filter
def get_range(value):
    """
    <ul>{% for i in 3|get_range %}
      <li>{{ i }}. Do something</li>
    {% endfor %}</ul>
    """
    return [val for val in xrange(value)]

@register.filter
def get_range1(value):
    return [val+1 for val in xrange(value)]

@register.filter
def get_index(value, idx):
    return value[idx]
