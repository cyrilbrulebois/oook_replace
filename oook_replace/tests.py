#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import tempfile, datetime, os
from zipfile import ZipFile, ZIP_DEFLATED

from django.conf import settings
from django.test import TestCase

from oook_replace import oook_replace

class OOOGenerationTest(TestCase):
    def testGeneration(self):
        tests_dir = os.path.abspath(os.getcwd() + "/../oook_replace/tests/")
        context = {'test_var':u"Testé & oui", 'test_var2':u"",
                   "test_date":datetime.date(2015, 1, 1),
                   "test_image":{'path':tests_dir+"/image.png"},
                   "test_table":{
                    'tablename':"MyTableName",
                    'header_rows':1,
                    'items':[
                       [{"value":"value in red", "backgroundcolor":"ff0000"},
                        {"value":"value", "backgroundcolor":"ff0000",
                         "rowspan":2}],
                       [{"value":"1", "colspan":2}, {"value":"2"}]
                      ]
                    },
                   "test_table2":{
                    'tablename':"MyTableName2",
                    'header_rows':1,
                    'items':[
                       [{"value":"value in red", "backgroundcolor":"ff0000"},
                        {"value":"value", "backgroundcolor":"ff0000",
                         "rowspan":2}],
                       [{"value":"1", "colspan":2}, {"value":"2"}]
                      ]
                    }
                   }
        tmp = tempfile.TemporaryFile()
        oook_replace(tests_dir + "/test-file.odt", tmp, context)
        inzip = ZipFile(tmp, 'r', ZIP_DEFLATED)
        value = inzip.read('content.xml')
        self.assertTrue("Test&#233; &amp; oui" in value)
        self.assertTrue("testé 2" not in value and "test&#233; 2" not in value)
        self.assertTrue("2015" in value)
        lg, ct = settings.LANGUAGE_CODE.split('-')
        if lg == 'fr':
            self.assertTrue('janvier' in value)
        if lg == 'en':
            self.assertTrue('january' in value)

        # table test
        self.assertTrue("ff0000" in value)
        self.assertTrue("value in red" in value)
        self.assertTrue("MyTableName" in value)
        self.assertTrue("table:number-columns-spanned" in value)
        self.assertTrue("table:number-rows-spanned" in value)
        self.assertTrue("table:table-header-rows" in value)

        # image
        self.assertTrue("Pictures"+os.sep+"image.png" in value)
        self.assertTrue("draw:image" in value)
