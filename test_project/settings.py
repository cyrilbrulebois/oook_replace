import os
DEBUG = True

SECRET_KEY='secret key! yes app only for testing purpose'

DATABASES = {
    'default':
        {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/tmp/oook_replace.db'
    }
}

INSTALLED_APPS = (
    'django.contrib.auth', 
    'django.contrib.contenttypes', 
    'django.contrib.sessions', 
    'django.contrib.sites',
    'django.contrib.admin',
    'oook_replace',
)
TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'templates'),
)

SITE_ID = 1
ROOT_URLCONF = 'urls'

LANGUAGE_CODE = 'fr-fr'

OOOK_DATE_FORMAT = "%-d %B %Y"

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
)

PAGE_DIM = (21, 29.7) # A4
